import React, { createContext, useState } from "react";
import { IntlProvider } from "react-intl";
import Turkish from "../lang/tr.json";
import Spanish from "../lang/es.json";
import English from "../lang/en.json";

export const I18NContext = createContext();

const local = navigator.language;

var lang;
if (local === "en") {
  lang = English;
} else {
  if (local === "tr") {
    lang = Turkish;
  } else {
    lang = Spanish;
  }
}
const I18NProvider = ({ children }) => {
  const [locale, setLocale] = useState(local);
  const [messages, setMessages] = useState(lang);
  function selectLanguage(newLocale) {
    setLocale(newLocale);
    if (newLocale === "en") {
      setMessages(English);
    } else {
      if (newLocale === "tr") {
        setMessages(Turkish);
      } else {
        setMessages(Spanish);
      }
    }
  }
  return (
    <I18NContext.Provider value={{ locale, selectLanguage }}>
      <IntlProvider messages={messages} locale={locale}>
        {children}
      </IntlProvider>
    </I18NContext.Provider>
  );
};
export default I18NProvider;

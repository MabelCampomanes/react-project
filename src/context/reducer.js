let user = localStorage.getItem("currentUser")
  ? (localStorage.getItem("currentUser"))
  : "";
  let originCountry = localStorage.getItem("originCountry")
  ? (localStorage.getItem("originCountry"))
  : ""; 
let token = localStorage.getItem("token")
  ? JSON.parse(localStorage.getItem("token"))
  : "";

// Estado inicial de la función reductora (puedo incluir x variables a diferencia del useState)
export const initialState = {
  user: "" || user,
  originCountry: "" || originCountry,
  token: "" || token,
  loading: false,
  errorMessage: null
};

// Función reductora (estado inicial, acciónes)
export const AuthReducer = (state, action) => {
  switch (action.type) {
    case "REQUEST_LOGIN":
      return {
        ...state,
        loading: true
      };
    case "LOGIN_SUCCESS":

      return {
        ...state,
        user: localStorage.getItem("currentUser"),
        originCountry: localStorage.getItem("originCountry"),
        token: action.payload.token,
        loading: false
      };
    case "LOGOUT":
      return {
        ...state,
        user: "",
        token: "",
      };

    case "LOGIN_ERROR":
      return {
        ...state,
        loading: false,
        errorMessage: action.error
      };

    default:
      throw new Error(`Unhandled action type: ${action.type}`);
  }
};
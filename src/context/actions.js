const ROOT_URL = 'https://fakestoreapi.com/auth';
 
export async function loginUser(dispatch, payload) {

  const requestOptions = {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(payload),
  };

  try {
    dispatch({ type: 'REQUEST_LOGIN' });
    const response = await fetch(`${ROOT_URL}/login`, requestOptions);
    const data = await response.json();
    

    if (data) {
      dispatch({ type: 'LOGIN_SUCCESS', payload: data});
       localStorage.setItem('token', JSON.stringify(data.token));
       localStorage.setItem('currentUser', payload.username);
       localStorage.setItem('originCountry', payload.originCountry);   

      return data;
      
    }

    dispatch({ type: 'LOGIN_ERROR', error: "No ha sido posible iniciar sesión" });
    return;
  } catch (error) {
    dispatch({ type: 'LOGIN_ERROR', error: error });
  }
}
 
export async function logout(dispatch) {
  dispatch({ type: 'LOGOUT' });
  localStorage.removeItem('currentUser');
  localStorage.removeItem('token');
  localStorage.removeItem('originCountry');
}
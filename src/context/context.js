import React, { useContext, useReducer } from 'react';
import { initialState, AuthReducer } from './reducer';

//Custom Hooks:
/// Contexto dónde se guardan los datos del usuario
const AuthStateContext = React.createContext();
/// Countexto de los despachadores que cambiarán el estado del usuario
const AuthDispatchContext = React.createContext();
 


// Estado del User
export function useAuthState() {
  const context = useContext(AuthStateContext);
  if (context === undefined) {
    throw new Error("useAuthState debe utilizarse dentro de un AuthProvider");
  }
 
  return context;
}

// Acciones de la función reductora
export function useAuthDispatch() {
  const context = useContext(AuthDispatchContext);
  if (context === undefined) {
    throw new Error("useAuthDispatch debe utilizarse dentro de un AuthProvider");
  }
 
  return context;
}


//Combinamos los contextos en un único Provider (authprovider)
export const AuthProvider = ({ children }) => {
  const [user, dispatch] = useReducer(AuthReducer, initialState);

    return (
    <AuthStateContext.Provider value={user}>
      <AuthDispatchContext.Provider value={dispatch}>
        {children}
      </AuthDispatchContext.Provider>
    </AuthStateContext.Provider>
  );
};


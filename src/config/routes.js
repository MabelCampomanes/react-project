// Librerías
import React from 'react';
import LoginForm from '../pages/LoginForm';

//Pages
import { Countries } from '../pages/Countries';
import NotFound from '../pages/NotFound';
import Country from '../pages/Country';

const routes = [
    {
        path:"/",
        element: <Countries />
    },
    {
        path:"/countries/:name",
        element: <Country />
    },
    {
        path:"/login",
        element: <LoginForm />
    },
    {
        path:"/*",
        element: <NotFound />
    },
];

export default routes;

import React from "react";

// Pages auth
import Country from '../pages/Country'

const authRoutes = [
  {
    path: "/countries/:name",
    element: <Country />,
  },
];

export default authRoutes;

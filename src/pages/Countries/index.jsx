// Librerías React y Estilo
import React, { useState, useEffect} from 'react';
import styles from './countries.module.scss';
import { Link } from 'react-router-dom';


// Contextos
import {useAuthState } from '../../context';


export const Countries = (props) => {

  // Hooks
  let [countries, setCountries] = useState([]);
  let [isLoading, setIsLoading] = useState(false);
  const { token } = useAuthState();


  useEffect(() => {
    setIsLoading(true);
    fetch("https://restcountries.com/v3.1/all")
        .then(res => res.json())
        .then(data => setCountries(data.slice(0, 10)))
        .finally(() => setIsLoading(false));
  }, []);

  const loading = isLoading ? 'Loading .......' : null;

  return (
    <div >
        <div className={styles.countriesPageTitle}>
            <h1 className={styles.h1}>
              Country List
            </h1>          
        </div>
         <div className={styles.countriesPageDiv } >
        { loading }
        { countries.map((country) => (
            <div className={styles.countriesPageCard}>
                <img className="country-img" src={country.flags.svg} alt="bandera" />
                <h2> {country.name.common}</h2>
                <h3> Currencies </h3>
                <h4> {Object.keys(country.currencies)[0]}</h4>
                <h4> {Object.keys(country.currencies)[1]}</h4>
                <h4> {Object.keys(country.currencies)[2]}</h4>
                <Link to={ !token ? "/login" : `/countries/${country.name.common}`} >
                      <button className={styles.btnCountries}> See Country Details </button>
                </Link>
            </div> 
        ))}
    </div>
    </div>
  )
}

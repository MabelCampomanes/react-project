// Librería - React + stilos
import React, { useState } from 'react';
import styles from './loginform.module.scss'
import { useNavigate } from "react-router-dom";

// Importación de contextos
import { loginUser, useAuthState, useAuthDispatch } from '../../context';
 
function LoginForm(props) {
  const [username, setuserName] = useState('')
  const [password, setPassword] = useState('')
  const [originCountry, setOriginCountry] = useState('')

  const dispatch = useAuthDispatch();
  const { loading, errorMessage } = useAuthState();

  let navigate = useNavigate();
  

  const handleLogin = async (e) => {
    e.preventDefault()
    let payload = { username, password, originCountry }
    console.log(payload);
    try {
      let response = await loginUser(dispatch, payload) 

      if (!response.token) return;
      navigate("/");

    } catch (error) {
      console.log(error)
    }

  }

  return (
    <div className={styles.container}>
      <div className={styles.formContainer}>

        {errorMessage ? <p className={styles.error}>{errorMessage}</p> : null}
        <form>

          <div className={styles.loginForm}>
            <div className={styles.loginFormItem}>
              <label htmlFor='username'>Username</label>
              <input 
                type='text' 
                id='username' 
                value={username} 
                onChange={(e) =>setuserName(e.target.value)}
                disabled={loading}
              />             
            </div>

            <div className={styles.loginFormItem}>
              <label htmlFor='password'>Password</label>
              <input 
                type='password' 
                id='password' 
                value={password} 
                onChange={(e) =>setPassword(e.target.value)}
                disabled={loading}
              />              
            </div>

            <div className={styles.loginFormItem}>
              <label htmlFor='password'>Country</label>
              <input 
                type='originCountry' 
                id='originCountry' 
                value={originCountry} 
                onChange={(e) =>setOriginCountry(e.target.value)}
                disabled={loading}
              />              
            </div>

          </div>

          <button onClick={handleLogin} disabled={loading}>Login</button>


        </form>

      </div>
    </div>
  );
}
 
export default LoginForm;
import React, { useEffect, useState} from 'react';
import { Link, useParams } from 'react-router-dom';
import styles from './country.module.scss';

//Componentes
import ConversorForm from '../../components/ConversorForm'

function Country (props) {

  let [countryCard, setCountryCard] = useState([]);
  let [isLoading, setIsLoading] = useState(false);
  const { name } = useParams();

  useEffect(() => {
    setIsLoading(true);
    fetch(`https://restcountries.com/v3.1/name/${name}`)
        .then(res => res.json())
        .then(data => setCountryCard(data))
        .finally(() => setIsLoading(false));
  }, [name]);

  const loading = isLoading ? 'Loading .......' : null;

 return (
  <div className={styles.countryPage}>
      <button >
        <Link to="/">Back</Link>
      </button>
      {loading}
      {countryCard.map((country) => (
        <div>
          <div>
                    <img className="country-img" src={country.flags.svg} alt="bandera" />
                    <h3> {country.name.common}</h3>
                    <h3> {Object.keys(country.currencies)[0]}</h3>
          </div>
          <ConversorForm currency2={Object.keys(country.currencies)[0]}/>
        </div>
      ))}

  </div>
  );
};
 
export default Country;
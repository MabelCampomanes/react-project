import React  from 'react'
import { Link } from 'react-router-dom';
import styles from '../Navbar/navbar.module.scss';
import { useNavigate } from "react-router-dom";
import Flag from 'react-world-flags'



//Componentes
import SearchButton from '../Navbar/SearchButton'
import { flagCodeFinder } from '../../utils/flagFinder';

// Contextos
import { logout, useAuthState, useAuthDispatch } from '../../context'

const Navbar = () => {

  // Contextos
  const { token, originCountry } = useAuthState();
  
  // FLAG 
  const flagCode = flagCodeFinder(originCountry);

  
  // BOTÓN CERRAR SESIÓN
    let navigate = useNavigate();
    const dispatch = useAuthDispatch();
    const handleLogout = () => {
    logout(dispatch); 
    navigate("/"); 
  };


  return (
    <div className={styles.container}>

        <div className="search">
          <SearchButton />

        </div>
        { !token ? 
          <Link to="/login"><button data-testid="flag-btn">Login</button></Link>       
        : 
        <div className={styles.menu}>
          <Flag className={styles.flag} code={flagCode.alpha3} height="20px" />
          <button className={styles.logoutBtn + ' ' + styles.btn} onClick={handleLogout}> Logout </button>
        </div>
         
        }

    </div>
  )
}

export default Navbar
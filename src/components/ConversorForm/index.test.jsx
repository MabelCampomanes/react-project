import { render, screen } from '@testing-library/react'
import { ConversorForm } from '../ConversorForm'

describe('ConversorForm', () => {
    
    const currency2 = "aed"
         
    it('renders properly', () => {
       
        render(<input value={currency2}> </input> )
        expect(screen.getByTestId(currency2.toUpperCase)).toBeInTheDocument()
    })
});
  

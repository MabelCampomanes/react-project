import React, { useContext } from "react";
import { useFormik } from "formik";
import styles from './conversorForm.module.scss';
import { FormattedMessage } from "react-intl";

// Contexto
import { useAuthState } from "../../context";
import { I18NContext } from '../../context/i18ncontext'

function ConversorForm (props)  {

    //Intl
    const { originCountry } = useAuthState();
    const { selectLanguage } = useContext(I18NContext);
    
    const spanishSpeakers = [ "Spain", "Venzuela", "Peru", "Argentina","Ecuador", "Uruguay", "Chile", "Mexico", "Colombia" ]
    if ( spanishSpeakers.includes(originCountry)){
      selectLanguage('es');
    } else if (originCountry ==="Turkey") {
      selectLanguage ('tr')
    } else { selectLanguage('en')}
    
    //Validaciones
    const validationProcess = (values) => {
      const errors = {};

    if (!values.currency1) {
      errors.currency1 = "Required";
    } else if (values.currency1.length !== 3) {
      errors.currency1 = "Must be 3 characters";
    }
  
    if (!values.amount1) {
      errors.password = "Required";
    } else if (values.amount1 === 0 ) {
      errors.amount1 = "Must not be 0 !!!";
    }
  
    if (!values.currency2) {
      errors.currency2 = "Required";
    } else if (values.currency2 === values.currency1) {
      errors.currency2 = "Differents currencies mandatory";
    }
  
    if (!values.currency2) {
      errors.currency2 = "Required";
    } else if (values.currency2.length !== 3 ) {
      errors.amount1 = "Must be 3 characters";
    } else if (values.currency2 === values.currency1) {
      errors.amount1 = "Must not be equal !!!";
    }

    return errors;
  };
    
  const { values, handleChange, handleBlur, handleSubmit, touched, errors } = useFormik({
    initialValues: {
      currency1: "",
      amount1: 0,
      currency2: props.currency2
    },
    validate: validationProcess,
    onSubmit: (values) => {
      console.log(values);
    },
  });



  return (
    <div className={styles.form}>

          <h2 data-testid="message.header">
          <FormattedMessage
            id="app.header"
            defaultMessage="Currency Converter"
          />
          </h2>

          <h4 data-testid="message.content">
          <FormattedMessage
            id="app.content"
            defaultMessage="Fill the form to see the results"
          />
          </h4>
          
      <form onSubmit={handleSubmit}>

      <label htmlFor="amount1">  </label>
        <input
         placeholder="Insert amount here"
          value={values.amount1}
          onChange={handleChange}
          onBlur={handleBlur}
          id="amount1"
          name="amount1"
          type="amount1"     
        />
        {touched.amount1 && errors.amount1 ? (
          <div className="error">{errors.amount1}</div>
        ) : null}

        <label htmlFor="currency1"> From </label>
        <input
          value={values.currency1}
          onChange={handleChange}
          onBlur={handleBlur}
          id="currency1"
          name="currency1"
        />
        {touched.currency1 && errors.currency1 ? (
          <div className="error">{ errors.currency1}</div>
        ) : null}

        <label htmlFor="currency2"> To </label>
        <input
          data-testid="currency2"
          value={values.currency2}
          onChange={handleChange}
          onBlur={handleBlur}
          id="currency2"
          name="currency2"                 
        />
        {touched.repassword && errors.currency2 ? (
          <div className="error">{errors.currency2}</div>
        ) : null}

        <button type="submit"> Show results</button>
      </form>
    </div>
  )
}

export default ConversorForm

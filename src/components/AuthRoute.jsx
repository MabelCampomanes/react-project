import { useAuthState } from "../context";
import { Navigate } from "react-router-dom";

function AuthRoute({ component }) {

  const { token } = useAuthState();
  
  return <>{token ? <>{component}</> : <Navigate to="/countries/:name" />}</>;
  
}

export default AuthRoute;

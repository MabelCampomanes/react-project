import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.scss';
import App from './App';
import { ChakraProvider, ColorModeScript } from '@chakra-ui/react';

import I18NProvider  from './context/i18ncontext'


const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
   <ChakraProvider>
     <ColorModeScript initialColorMode="light"></ColorModeScript>
      <I18NProvider>
        <App />
      </I18NProvider>
  </ChakraProvider>
</React.StrictMode>


);

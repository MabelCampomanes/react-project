//Librería - React
import './App.scss';
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";

//Chakra
import { IconButton } from "@chakra-ui/button";
import { useColorMode } from "@chakra-ui/color-mode";
import { Flex, VStack, Spacer } from "@chakra-ui/layout";
import { FaSun, FaMoon } from "react-icons/fa";

// Archivo de Rutas
import routes from './config/routes';
import authRoutes from './config/auth.routes';
import AuthRoute from './components/AuthRoute';

// Proveedor de los contextos
import { AuthProvider } from './context';
import Navbar from './components/Navbar';



function App() {

  const { colorMode, toggleColorMode } = useColorMode(); 
  const isDark = colorMode === "dark";

  return (
  <VStack>
      <Flex w="100%">
        
        <Spacer>
          <AuthProvider>
            <Router>
              <Navbar />
              <Routes>
                {routes.map((route) => (
                  
                    <Route
                      key={route.path}
                      path={route.path}
                      element={route.element}
                    />
                ))}
                {authRoutes.map((route) => (
                  <Route
                    key={route.path}
                    path={route.path}
                    element={<AuthRoute component={route.element} />}
                  />
                ))}
              </Routes>
            </Router>
          </AuthProvider> 
      </Spacer>
      <IconButton ml={9} icon={isDark ? <FaSun /> : <FaMoon />} 
       isRound="true" onClick={toggleColorMode}></IconButton>
      </Flex>
    </VStack>
  );
}

export default App;
